package ltw.pb.be.dto.response.auth;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AuthActiveResponse extends AuthSignInResponse {
  private String accessToken;
  private String refreshToken;

  public static AuthSignInResponse from(String accessToken, String refreshToken) {
    var authSignInResponse = new AuthActiveResponse();
    authSignInResponse.accessToken = accessToken;
    authSignInResponse.refreshToken = refreshToken;
    return authSignInResponse;
  }
}
