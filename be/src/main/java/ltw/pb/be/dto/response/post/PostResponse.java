package ltw.pb.be.dto.response.post;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PostResponse {
  private String id;
  private String createBy;
  private String createdAt;
  private String lastUpdatedBy;
  private String lastUpdatedAt;
  private String title;
  private String description;
  private String content;
  private String categoryId;
  private String categoryName;
}
