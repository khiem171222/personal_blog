package ltw.pb.be.dto.response.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ltw.pb.be.dto.response.role.RoleResponse;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class UserDetailResponse {
  private String id;
  private String name;
  private String username;
  private String email;
  @JsonIgnore
  private String password;
  private List<RoleResponse> roles = new ArrayList<>();
}
