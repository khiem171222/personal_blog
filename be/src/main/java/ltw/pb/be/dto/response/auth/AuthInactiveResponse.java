package ltw.pb.be.dto.response.auth;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AuthInactiveResponse extends AuthSignInResponse {
  private String messagse;

  public static AuthInactiveResponse from(String messagse) {
    var authInactiveResponse = new AuthInactiveResponse();
    authInactiveResponse.setMessagse(messagse);
    return authInactiveResponse;
  }
}
