package ltw.pb.be.dto.response.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PostPageResponse {
  private List<PostResponse> posts;
  private int countPosts;
}
