package ltw.pb.be.dto.response.auth;

import lombok.Data;

@Data
public class TokenResponse {
  private String accessToken;
  private String refreshToken;
  private String tokenType = "Bearer";

  public static TokenResponse of(String accessToken, String refreshToken) {
    var tokenResponseDTO = new TokenResponse();
    tokenResponseDTO.setAccessToken(accessToken);
    tokenResponseDTO.setRefreshToken(refreshToken);
    return tokenResponseDTO;
  }
}
