package ltw.pb.be.dto.request.auth;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ltw.pb.be.annotation.ValidationEmail;
import ltw.pb.be.annotation.ValidationPassword;


@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AuthRegisterRequest {
  @NotEmpty(message = "Username must be not empty")
  @NotNull(message = "Username must be not null")
  private String username;
  @ValidationPassword
  private String password;
  @NotEmpty(message = "full name must be not empty")
  @NotNull(message = "full name must be not null")
  private String name;
  @ValidationEmail
  private String email;
}
