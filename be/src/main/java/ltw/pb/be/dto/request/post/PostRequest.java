package ltw.pb.be.dto.request.post;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;


@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PostRequest {
  @NotEmpty
  @Size(min = 2, message = "Post title should have at least 2 characters")
  private String title;
  @NotEmpty
  @Size(min = 10, message = "Post description should have at least 10 characters")
  private String description;
  @NotEmpty
  private String content;
  private String categoryId;
}
