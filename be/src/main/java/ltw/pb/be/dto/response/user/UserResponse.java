package ltw.pb.be.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class UserResponse {
  private String name;
  private String username;
  private String email;
  private String password;
}
