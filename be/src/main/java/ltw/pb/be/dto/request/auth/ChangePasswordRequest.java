package ltw.pb.be.dto.request.auth;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;


@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ChangePasswordRequest {
  private String currentPassword;
  private String newPassword;
  private String confirmPassword;

  public boolean isPasswordMatch() {
    return newPassword.equals(confirmPassword);
  }
}