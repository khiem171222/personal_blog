package ltw.pb.be.dto.response.comment;

public class CommentResponse {
  private String name;
  private String username;
  private String body;
}
