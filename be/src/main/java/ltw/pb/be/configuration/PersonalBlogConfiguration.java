package ltw.pb.be.configuration;

import ltw.pb.be.repository.CategoryRepository;
import ltw.pb.be.repository.CommentRepository;
import ltw.pb.be.repository.PostRepository;
import ltw.pb.be.repository.UserRepository;
import ltw.pb.be.service.*;
import ltw.pb.be.service.impl.*;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import static ltw.pb.be.constanst.PersonalBlogConstants.CommonConstants.ENCODING_UTF_8;

@Configuration
public class PersonalBlogConfiguration {

  @Bean
  public MessageSource messageSource() {
    var messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:i18n/messages");
    messageSource.setDefaultEncoding(ENCODING_UTF_8);
    return messageSource;
  }

  @Bean
  public CategoryService categoryService(CategoryRepository categoryRepository) {
    return new CategoryServiceImpl(categoryRepository);
  }

  @Bean
  public CommentService commentService(CommentRepository commentRepository) {
    return new CommentServiceImpl(commentRepository);
  }

  @Bean
  public PostService postService(PostRepository postRepository, CategoryService categoryService) {
    return new PostServiceImpl(postRepository, categoryService);
  }

  @Bean
  public UserService userService(UserRepository userRepository) {
    return new UserServiceImpl(userRepository);
  }

  @Bean
  public AuthService authService(UserService userService, JwtTokenService jwtTokenService) {
    return new AuthServiceImpl(userService, jwtTokenService);
  }

  @Bean
  public JwtTokenService jwtTokenService() {
    return new JwtTokenServiceImpl();
  }

  @Bean
  public MessageService messageService(MessageSource messageSource) {
    return new MessageServiceImpl(messageSource);
  }

}
