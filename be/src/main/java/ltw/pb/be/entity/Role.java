package ltw.pb.be.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ltw.pb.be.entity.base.BaseEntity;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Table(name = "roles")
public class Role extends BaseEntity {
  private String name;
}
