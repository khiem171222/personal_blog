package ltw.pb.be.entity;

import jakarta.persistence.*;
import lombok.*;
import ltw.pb.be.entity.base.BaseEntity;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Table(name = "comments")
public class Comment extends BaseEntity {
  private String name;
  private String email;
  private String body;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "post_id", nullable = false)
  private Post post;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;
}
