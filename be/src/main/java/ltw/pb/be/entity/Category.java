package ltw.pb.be.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ltw.pb.be.entity.base.BaseEntity;

import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Table(name = "categories")
public class Category extends BaseEntity {
  private String name;
  private String description;
  @OneToMany(mappedBy = "category")
  private List<Post> posts;
}
