package ltw.pb.be.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ltw.pb.be.entity.base.BaseEntity;

import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Table(name = "posts")
public class Post extends BaseEntity {
  @Column(name = "title", nullable = false)
  private String title;
  @Column(name = "description", nullable = false)
  private String description;
  @Column(name = "content", nullable = false)
  private String content;
  @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Comment> comments = new HashSet<>();
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "category_id")
  private Category category;
  public static Post of(String title, String description, String content, Category category){
    var post = new Post();
    post.setTitle(title);
    post.setDescription(description);
    post.setContent(content);
    post.setCategory(category);
    return post;
  }
}
