package ltw.pb.be.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.category.CategoryRequest;
import ltw.pb.be.dto.response.ResponseGeneral;
import ltw.pb.be.dto.response.category.CategoryResponse;
import ltw.pb.be.service.CategoryService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ltw.pb.be.constanst.PersonalBlogConstants.CommonConstants.SUCCESS;
import static ltw.pb.be.constanst.PersonalBlogConstants.MessageCode.CREATE_CATEGORY_SUCCESS;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/categories")
public class CategoryController {
  private final CategoryService categoryService;

  @PostMapping
  public ResponseGeneral<CategoryResponse> create(@RequestBody CategoryRequest request) {
    log.info("(create)request: {}", request);

    return ResponseGeneral.ofCreated(
          CREATE_CATEGORY_SUCCESS,
          categoryService.create(request)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<CategoryResponse> delete(@PathVariable String id) {
    log.info("(delete)id: {}", id);

    categoryService.remove(id);
    return ResponseGeneral.ofSuccess(
          SUCCESS
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<CategoryResponse> get(@PathVariable String id) {
    log.info("(get)id: {}", id);

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          categoryService.detail(id)
    );
  }

  @GetMapping
  public ResponseGeneral<List<CategoryResponse>> list() {
    log.info("(list)start");

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          categoryService.getAll()
    );
  }

  @PutMapping("{id}")
  public ResponseGeneral<CategoryResponse> update(@PathVariable String id, @RequestBody CategoryRequest request) {
    log.info("(get)id: {}", id);

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          categoryService.update(id, request)
    );
  }
}
