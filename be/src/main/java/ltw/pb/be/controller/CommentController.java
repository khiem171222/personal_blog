package ltw.pb.be.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.comment.CommentRequest;
import ltw.pb.be.dto.response.ResponseGeneral;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/posts")
public class CommentController {

  @PostMapping("{postId}/comments")
  public ResponseGeneral createComment(
        @PathVariable(value = "postId") String postId,
        @Valid @RequestBody CommentRequest request
  ) {
    return null;
  }

  @GetMapping("{postId}/comments")
  public ResponseGeneral getCommentsByPostId(@PathVariable(value = "postId") Long postId) {
    return null;
  }

  @GetMapping("{postId}/comments/{id}")
  public ResponseGeneral getCommentById(
        @PathVariable(value = "postId") String postId,
        @PathVariable(value = "id") String commentId
  ) {
    return null;
  }

  @PutMapping("{postId}/comments/{id}")
  public ResponseGeneral updateComment(
        @PathVariable(value = "postId") String postId,
        @PathVariable(value = "id") String commentId,
        @Valid @RequestBody CommentRequest request
  ) {
    return null;
  }

  @DeleteMapping("{postId}/comments/{id}")
  public ResponseEntity<String> deleteComment(
        @PathVariable(value = "postId") String postId,
        @PathVariable(value = "id") String commentId
  ) {
    return null;
  }
}
