package ltw.pb.be.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.post.PostRequest;
import ltw.pb.be.dto.response.ResponseGeneral;
import ltw.pb.be.dto.response.post.PostPageResponse;
import ltw.pb.be.dto.response.post.PostResponse;
import ltw.pb.be.service.PostService;
import org.springframework.web.bind.annotation.*;

import static ltw.pb.be.constanst.PersonalBlogConstants.CommonConstants.*;

@Slf4j
@RestController
@RequestMapping("api/v1/posts")
@RequiredArgsConstructor
public class PostController {
  private final PostService postService;

  @PostMapping
  public ResponseGeneral<PostResponse> create(@Valid @RequestBody PostRequest request) {
    log.info("(create)request: {}", request);

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          postService.create(request)
    );
  }

  @GetMapping
  public ResponseGeneral<PostPageResponse> list(
        @RequestParam(value = "pageNo", defaultValue = DEFAULT_PAGE_NUMBER, required = false) int pageNo,
        @RequestParam(value = "pageSize", defaultValue = DEFAULT_PAGE_SIZE, required = false) int pageSize,
        @RequestParam(value = "isAll", defaultValue = DEFAULT_ISALL, required = false) boolean isAll,
        @RequestParam(value = "sortBy", defaultValue = DEFAULT_SORT_BY, required = false) String sortBy,
        @RequestParam(value = "sortDir", defaultValue = DEFAULT_SORT_DIRECTION, required = false) String sortDir,
        @RequestParam(value = "categoryId", required = false) String categoryId
  ) {
    log.info("(list)pageNo: {}, pageSize: {}, isAll: {}, sortBy: {}, sortDir: {}, categoryId: {}",
          pageNo, pageSize, isAll, sortBy, sortDir, categoryId
    );

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          postService.list(pageNo, pageSize, isAll, sortBy, sortDir, categoryId)
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<PostResponse> get(@PathVariable String id) {
    log.info("(get)id: {}", id);
    return ResponseGeneral.ofSuccess(
          SUCCESS,
          postService.detail(id)
    );
  }

  @PutMapping("{id}")
  public ResponseGeneral<PostResponse> updatePost(
        @PathVariable(name = "id") String id,
        @Valid @RequestBody PostRequest request
  ) {
    log.info("(create)id: {}, request: {}", id, request);
    return ResponseGeneral.ofSuccess(
          SUCCESS,
          postService.update(id, request)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(@PathVariable String id) {
    log.info("(delete)id: {}", id);

    postService.remove(id);
    return ResponseGeneral.ofSuccess(
          SUCCESS
    );
  }
}
