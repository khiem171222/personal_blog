package ltw.pb.be.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.auth.AuthRegisterRequest;
import ltw.pb.be.dto.request.auth.AuthSignInRequest;
import ltw.pb.be.dto.request.auth.RefreshTokenRequest;
import ltw.pb.be.dto.response.ResponseGeneral;
import ltw.pb.be.service.AuthService;
import org.springframework.web.bind.annotation.*;

import static ltw.pb.be.constanst.PersonalBlogConstants.CommonConstants.SUCCESS;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
public class AuthController {
  private final AuthService authService;

  @PostMapping("login")
  public ResponseGeneral login(
        @RequestBody AuthSignInRequest request
  ) {
    log.info("(login)request: {}", request);
    return ResponseGeneral.ofSuccess(
          SUCCESS,
          authService.signIn(
                request.getUsername(),
                request.getPassword()
          )
    );
  }

  @PostMapping("register")
  public ResponseGeneral register(@RequestBody AuthRegisterRequest request) {
    log.info("(register)request: {}", request);

    return ResponseGeneral.ofSuccess(
          SUCCESS,
          authService.register(request)
    );
  }

  @PostMapping("/refresh-token")
  public ResponseGeneral refreshToken(@RequestBody RefreshTokenRequest request) {
    log.info("(refreshToken)request: {}", request);
    return ResponseGeneral.ofSuccess(
          SUCCESS,
          authService.refreshToken(request)
    );
  }
}
