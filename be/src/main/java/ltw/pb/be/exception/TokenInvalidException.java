package ltw.pb.be.exception;

import ltw.pb.be.exception.base.BadRequestException;

public class TokenInvalidException extends BadRequestException {
  public TokenInvalidException() {
    setCode("Token is Invalid");
  }
}
