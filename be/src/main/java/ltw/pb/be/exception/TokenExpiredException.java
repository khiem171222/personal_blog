package ltw.pb.be.exception;

import ltw.pb.be.exception.base.BadRequestException;

public class TokenExpiredException extends BadRequestException {
    public TokenExpiredException() {
        setCode("Token is Expired");
    }
}
