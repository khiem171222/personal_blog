package ltw.pb.be.service.impl;

import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.exception.TokenExpiredException;
import ltw.pb.be.exception.TokenInvalidException;
import ltw.pb.be.service.JwtTokenService;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Slf4j
@RequiredArgsConstructor
@Component
public class JwtTokenServiceImpl implements JwtTokenService {
  @Value("${token.key}")
  private String key;
  @Value("${token.expire-time-access-token}")
  private long expireTimeAccessToken;
  @Value("${token.expire-time-refresh-token}")
  public long expireTimeRefreshToken;

  private String generateToken(String subject, Map<String, Object> claims, long tokenLifeTime) {
    log.info("(generateToken)start");
    return Jwts.builder()
          .setSubject(subject)
          .claim("claims", claims)
          .setIssuedAt(new Date(System.currentTimeMillis()))
          .setExpiration(new Date(System.currentTimeMillis() + tokenLifeTime))
          .signWith(SignatureAlgorithm.HS256, key)
          .compact();
  }

  public String generateRefreshToken(String userId, String username) {
    log.info("(generateRefreshToken)start");
    var claims = new HashMap<String, Object>();
    claims.put("username", username);
    return generateToken(userId, claims, expireTimeAccessToken);
  }

  public String generateAccessToken(String userId, Map<String, Object> claims) {
    log.info("(generateAccessToken)start");
    return generateToken(userId, claims, expireTimeRefreshToken);
  }

  public void validateToken(String token) {
    log.info("(validateToken)start");
    if (!isValidToken(token)) {
      log.error("(validateToken) ==========> TokenInvalidException");
      throw new TokenInvalidException();
    }
    if (isExpiredToken(token)) {
      log.error("(validateToken) ==========> TokenExpiredException");
      throw new TokenExpiredException();
    }
  }

  public String getSubjectFromToken(String token) {
    validateToken(token);
    log.info("(getSubjectFromToken)start");
    return getClaims(token, key).getSubject();
  }

  public String getUsernameFromToken(String token) {
    validateToken(token);
    log.info("(getUsernameFromToken) start");
    Map<String, Object> claims = (Map<String, Object>) getClaims(token, key).get("claims");
    return String.valueOf(claims.get("username"));
  }

  public Object getRoleFromToken(String token) {
    validateToken(token);
    log.info("(getRoleFromToken)start");
    Map<String, Object> claims = (Map<String, Object>) getClaims(token, key).get("claims");
    return claims.get("roles");
  }

  public Boolean isValidToken(String token) {
    try {
      Jwts.parser().setSigningKey(key).parseClaimsJws(token);
      return true;
    } catch (JwtException | IllegalArgumentException e) {
      return false;
    }
  }

  public Boolean isExpiredToken(String token) {
    return getClaims(token, key).getExpiration().before(new Date());
  }

  private Claims getClaims(String token, String secretKey) {
    log.info("(getClaims)start");
    return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
  }
}
