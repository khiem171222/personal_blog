package ltw.pb.be.service.impl;


import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.post.PostRequest;
import ltw.pb.be.dto.response.post.PostPageResponse;
import ltw.pb.be.dto.response.post.PostResponse;
import ltw.pb.be.entity.Post;
import ltw.pb.be.exception.base.ConflictException;
import ltw.pb.be.exception.base.NotFoundException;
import ltw.pb.be.repository.PostRepository;
import ltw.pb.be.service.CategoryService;
import ltw.pb.be.service.PostService;
import ltw.pb.be.service.base.BaseServiceImpl;
import ltw.pb.be.utils.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public class PostServiceImpl extends BaseServiceImpl<Post> implements PostService {
  private final PostRepository repository;
  private final CategoryService categoryService;

  public PostServiceImpl(PostRepository repository, CategoryService categoryService) {
    super(repository);
    this.repository = repository;
    this.categoryService = categoryService;
  }

  @Override
  @Transactional
  public PostResponse create(PostRequest request) {
    log.info("(create)request: {}", request);

    checkPostExist(request.getTitle());
    Post post = toEntity(request);
    return postDto(save(post));
  }

  @Override
  public PostResponse detail(String id) {
    log.info("(detail)id: {}", id);
    return postDto(find(id));
  }

  @Override
  public PostPageResponse list(
        int pageNo, int pageSize,
        boolean isAll, String sortBy,
        String sortDir, String categoryId
  ) {
    log.info("(list)pageNo: {}, pageSize: {}, isAll: {}, sortBy: {}, sortDir: {}, categoryId: {}",
          pageNo, pageSize, isAll, sortBy, sortDir, categoryId
    );

    Pageable pageable = PageRequest.of(
          pageNo,
          pageSize,
          sortBy.equals("DESC") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending());

    List<Post> posts = isAll ? repository.list(categoryId) : repository.listPage(categoryId, pageable);
    List<PostResponse> postsResponse = posts.stream().map(this::postDto).collect(Collectors.toList());

    return PostPageResponse.of(
          postsResponse,
          repository.countList(categoryId)
    );
  }

  @Override
  @Transactional
  public PostResponse update(String id, PostRequest request) {
    log.info("(update)id: {}, request: {}", id, request);

    var existedPost = find(id);
    if (!existedPost.getTitle().equals(request.getTitle())) checkPostExist(request.getTitle());
    updateField(existedPost, request);
    return postDto(update(existedPost));
  }

  @Override
  @Transactional
  public void remove(String id) {
    log.info("(remove)id: {}", id);
    repository.delete(find(id));
  }

  public Post find(String id) {
    log.info("(find)id: {}", id);
    return repository.findById(id).orElseThrow(NotFoundException::new);
  }

  private PostResponse postDto(Post post) {
    log.info("(postDto)start");

    return PostResponse.of(
          post.getId(),
          post.getCreateBy(),
          post.getCreatedAt(),
          post.getLastUpdatedBy(),
          post.getLastUpdatedAt(),
          post.getTitle(),
          post.getDescription(),
          post.getContent(),
          post.getCategory().getId(),
          post.getCategory().getName()
    );
  }

  private Post toEntity(PostRequest request) {
    log.info("(toEntity)request: {}", request);

    return Post.of(
          request.getTitle(),
          request.getDescription(),
          request.getContent(),
          !StringUtils.isNullOrEmpty(request.getCategoryId()) ? categoryService.find(request.getCategoryId()) : null
    );
  }

  void updateField(Post post, PostRequest request) {
    log.info("(updateField)post: {}, request: {}", post, request);

    post.setTitle(request.getTitle());
    post.setDescription(request.getDescription());
    post.setContent(request.getContent());
    post.setCategory(!StringUtils.isNullOrEmpty(request.getCategoryId()) ?
          categoryService.find(request.getCategoryId()) : post.getCategory());
  }

  private void checkPostExist(String title) {
    log.info("(checkPostExist)title: {}", title);

    if (repository.existsByTitle(title)) {
      log.error("(checkCategoryExist) ==========> ConflictException", title);
      throw new ConflictException();
    }
  }

}
