package ltw.pb.be.service;

public interface MessageService {
  String getMessage(String code, String language);
}
