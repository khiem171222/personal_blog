package ltw.pb.be.service;


import ltw.pb.be.dto.request.category.CategoryRequest;
import ltw.pb.be.dto.response.category.CategoryResponse;
import ltw.pb.be.entity.Category;
import ltw.pb.be.service.base.BaseService;

import java.util.List;

public interface CategoryService extends BaseService<Category> {
  CategoryResponse create(CategoryRequest request);

  CategoryResponse detail(String id);

  List<CategoryResponse> getAll();

  CategoryResponse update(String id, CategoryRequest request);

  void remove(String id);
  Category find(String id);
}
