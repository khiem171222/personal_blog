package ltw.pb.be.service.impl;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.auth.AuthRegisterRequest;
import ltw.pb.be.dto.request.auth.RefreshTokenRequest;
import ltw.pb.be.dto.request.user.UserRequest;
import ltw.pb.be.dto.response.auth.AuthActiveResponse;
import ltw.pb.be.dto.response.auth.AuthRegisterResponse;
import ltw.pb.be.dto.response.auth.AuthSignInResponse;
import ltw.pb.be.dto.response.auth.TokenResponse;
import ltw.pb.be.dto.response.role.RoleResponse;
import ltw.pb.be.dto.response.user.UserDetailResponse;
import ltw.pb.be.exception.base.BadRequestException;
import ltw.pb.be.service.AuthService;
import ltw.pb.be.service.JwtTokenService;
import ltw.pb.be.service.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ltw.pb.be.utils.MapperUtils.MODEL_MAPPER;
import static ltw.pb.be.utils.PasswordEncoderUtils.PASSWORD_ENCODER;


@Slf4j
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
  private final UserService userService;
  private final JwtTokenService jwtTokenService;

  @Override
  public AuthSignInResponse signIn(String username, String password) {
    log.info("(signIn)username: {}, password: {}", username, password);
    var user = userService.getDetailByUsername(username);
    log.info("(signIn)user: {}", user);
    if (!PASSWORD_ENCODER.matches(password, user.getPassword())) {
      throw new BadRequestException();
    }

    String accessToken = jwtTokenService.generateAccessToken(
          user.getId(),
          createClaim(user.getUsername(), user.getRoles())
    );
    String refreshToken = jwtTokenService.generateRefreshToken(user.getId(), user.getUsername());
    List<GrantedAuthority> grantedAuthority = getGrantedAuthority(user);
    authenticate(user.getUsername(), user.getId(), grantedAuthority);

    return AuthActiveResponse.from(accessToken, refreshToken);
  }

  public AuthRegisterResponse register(AuthRegisterRequest request) {
    log.info("(register) request: {}", request);
    var user = userService.create(MODEL_MAPPER.map(request, UserRequest.class));
    return MODEL_MAPPER.map(user, AuthRegisterResponse.class);
  }

  @Override
  public TokenResponse refreshToken(RefreshTokenRequest request) {
    log.info("(refreshToken)request: {}", request);
    String userId = jwtTokenService.getSubjectFromToken(request.getRefreshToken());
    var user = userService.getDetailById(userId);

    String accessToken = jwtTokenService.generateAccessToken(
          user.getId(),
          createClaim(user.getUsername(), user.getRoles())
    );

    return TokenResponse.of(accessToken, request.getRefreshToken());
  }

  private void authenticate(String username, String userId, List<GrantedAuthority> grantedAuthorities) {
    log.info("(authenticate)username: {}, userid: {}, grantedAuthorities:{}", username, userId, grantedAuthorities);
    var user = userService.getById(userId);
    var usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(
          username,
          userId,
          grantedAuthorities
    );
    usernamePasswordAuthToken.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthToken);
  }

  private Map<String, Object> createClaim(
        String username,
        List<RoleResponse> roles
  ) {
    log.info("(createClaim)username: {}, roles: {}", username, roles);

    Map<String, Object> claims = new HashMap<>();
    List<String> rolesName = roles.stream().map(role -> role.getName()).collect(Collectors.toList());
    claims.put("username", username);
    claims.put("roles", rolesName.size() > 0 ? rolesName : new ArrayList<>());
    return claims;
  }


  private List<GrantedAuthority> getGrantedAuthority(UserDetailResponse user) {
    List<GrantedAuthority> grantedAuthority = new ArrayList<>();
    user.getRoles().stream()
          .forEach(role -> grantedAuthority.add(new SimpleGrantedAuthority(role.getName())));
    return grantedAuthority;
  }
}
