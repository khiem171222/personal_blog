package ltw.pb.be.service.impl;


import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.category.CategoryRequest;
import ltw.pb.be.dto.response.category.CategoryResponse;
import ltw.pb.be.entity.Category;
import ltw.pb.be.exception.base.ConflictException;
import ltw.pb.be.exception.base.NotFoundException;
import ltw.pb.be.repository.CategoryRepository;
import ltw.pb.be.service.CategoryService;
import ltw.pb.be.service.base.BaseServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

import static ltw.pb.be.utils.MapperUtils.MODEL_MAPPER;

@Slf4j
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {
  private final CategoryRepository repository;

  public CategoryServiceImpl(CategoryRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public CategoryResponse create(CategoryRequest request) {
    log.info("(create)request: {}", request);

    checkCategoryExist(request.getName());
    Category category = MODEL_MAPPER.map(request, Category.class);
    return MODEL_MAPPER.map(save(category), CategoryResponse.class);
  }

  @Override
  public CategoryResponse detail(String id) {
    log.info("(detail)id: {}", id);

    return MODEL_MAPPER.map(find(id), CategoryResponse.class);
  }

  @Override
  public List<CategoryResponse> getAll() {
    log.info("(getAll)start");

    List<Category> categories = repository.findAll();

    return categories.stream()
          .map(address -> MODEL_MAPPER.map(address, CategoryResponse.class))
          .collect(Collectors.toList());
  }

  @Override
  public CategoryResponse update(String id, CategoryRequest request) {
    log.info("(update)id: {}, request: {}", id, request);

    var existedCategory = find(id);
    if (!request.getName().equals(existedCategory.getName())) checkCategoryExist(request.getName());
    existedCategory.setName(request.getName());
    existedCategory.setDescription(request.getDescription());

    return MODEL_MAPPER.map(save(existedCategory), CategoryResponse.class);
  }

  @Override
  public void remove(String id) {
    log.info("(remove)id: {}", id);

    repository.delete(find(id));
  }

  private void checkCategoryExist(String name) {
    log.info("(checkCategoryExist)name: {}", name);

    if (repository.existsByName(name)) {
      log.error("(checkCategoryExist) ==========> ConflictException", name);
      throw new ConflictException();
    }
  }

  public Category find(String id) {
    log.info("(find)id: {}", id);
    return repository.findById(id).orElseThrow(NotFoundException::new);
  }

}
