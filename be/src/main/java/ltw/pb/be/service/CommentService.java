package ltw.pb.be.service;


import ltw.pb.be.dto.request.comment.CommentRequest;
import ltw.pb.be.dto.response.comment.CommentResponse;
import ltw.pb.be.entity.Comment;
import ltw.pb.be.service.base.BaseService;

import java.util.List;

public interface CommentService extends BaseService<Comment> {
  CommentResponse create(String postId, CommentRequest request);

  List<CommentResponse> getByPostId(String postId);

  CommentResponse getById(String commentId);

  CommentResponse update(String postId, String commentId, CommentRequest request);

  void delete(String postId, String commentId);
}
