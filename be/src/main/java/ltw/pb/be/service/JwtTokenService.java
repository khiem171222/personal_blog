package ltw.pb.be.service;

import java.util.Map;

public interface JwtTokenService {
  String generateRefreshToken(String userId, String username);

  String generateAccessToken(String userId, Map<String, Object> claims);

  void validateToken(String token);

  String getSubjectFromToken(String token);

  String getUsernameFromToken(String token);
  Boolean isValidToken(String token);

  Boolean isExpiredToken(String token);

  Object getRoleFromToken(String token);
}
