package ltw.pb.be.service.base;

import java.util.List;

public interface BaseService<T> {
  T save(T t);
  T update(T t);
  void delete(String id);
  T get(String id);
  List<T> list();
}
