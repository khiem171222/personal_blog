package ltw.pb.be.service;

import ltw.pb.be.dto.request.user.UserRequest;
import ltw.pb.be.dto.response.user.UserDetailResponse;
import ltw.pb.be.dto.response.user.UserResponse;
import ltw.pb.be.entity.User;
import ltw.pb.be.service.base.BaseService;

import java.util.List;

public interface UserService extends BaseService<User> {
  UserResponse create(UserRequest request);
  UserResponse getById(String id);
  UserDetailResponse getDetailById(String id);
  UserDetailResponse getDetailByUsername(String username);
  UserResponse update(String id);
  List<UserResponse> list(int page, int size, boolean isAll);
  void remove(String id);
  public User find(String id);
}
