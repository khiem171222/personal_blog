package ltw.pb.be.service;


import ltw.pb.be.dto.request.auth.AuthRegisterRequest;
import ltw.pb.be.dto.request.auth.RefreshTokenRequest;
import ltw.pb.be.dto.response.auth.AuthRegisterResponse;
import ltw.pb.be.dto.response.auth.AuthSignInResponse;
import ltw.pb.be.dto.response.auth.TokenResponse;

public interface AuthService {
  AuthSignInResponse signIn(String username, String password);

  AuthRegisterResponse register(AuthRegisterRequest request);

  TokenResponse refreshToken(RefreshTokenRequest request);

}
