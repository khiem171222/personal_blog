package ltw.pb.be.service.impl;

import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.comment.CommentRequest;
import ltw.pb.be.dto.response.comment.CommentResponse;
import ltw.pb.be.entity.Comment;
import ltw.pb.be.repository.CommentRepository;
import ltw.pb.be.service.CommentService;
import ltw.pb.be.service.base.BaseServiceImpl;

import java.util.List;
@Slf4j
public class CommentServiceImpl extends BaseServiceImpl<Comment> implements CommentService {
  private final CommentRepository commentRepository;
  public CommentServiceImpl(CommentRepository commentRepository) {
    super(commentRepository);
    this.commentRepository = commentRepository;
  }

  @Override
  public CommentResponse create(String postId, CommentRequest request) {
    Comment comment = new Comment();
    return null;
  }

  @Override
  public List<CommentResponse> getByPostId(String postId) {
    return null;
  }

  @Override
  public CommentResponse getById(String commentId) {
    return null;
  }

  @Override
  public CommentResponse update(String postId, String commentId, CommentRequest request) {
    return null;
  }

  @Override
  public void delete(String postId, String commentId) {

  }
}
