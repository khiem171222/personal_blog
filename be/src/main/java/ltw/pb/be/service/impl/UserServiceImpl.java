package ltw.pb.be.service.impl;

import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.dto.request.user.UserRequest;
import ltw.pb.be.dto.response.user.UserDetailResponse;
import ltw.pb.be.dto.response.user.UserResponse;
import ltw.pb.be.entity.Category;
import ltw.pb.be.entity.User;
import ltw.pb.be.exception.base.ConflictException;
import ltw.pb.be.exception.base.NotFoundException;
import ltw.pb.be.repository.UserRepository;
import ltw.pb.be.service.UserService;
import ltw.pb.be.service.base.BaseServiceImpl;

import java.util.List;

import static ltw.pb.be.utils.MapperUtils.MODEL_MAPPER;
import static ltw.pb.be.utils.PasswordEncoderUtils.PASSWORD_ENCODER;

@Slf4j
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
  private final UserRepository repository;

  public UserServiceImpl(UserRepository repository) {
    super(repository);
    this.repository = repository;
  }


  @Override
  public UserResponse create(UserRequest request) {
    log.info("(create) request: {}", request);

    validateExistByUserName(request.getUsername());
    validateExistByEmail(request.getEmail());
    User user = User.from(
          request.getUsername(),
          PASSWORD_ENCODER.encode(request.getPassword()),
          request.getUsername(),
          request.getName()
    );

    return MODEL_MAPPER.map(save(user), UserResponse.class);
  }

  @Override
  public UserResponse getById(String id) {
    log.info("(getById)id: {}", id);
    return MODEL_MAPPER.map(find(id), UserResponse.class);
  }

  @Override
  public UserDetailResponse getDetailById(String id) {
    log.info("(getDetail)id: {}", id);
    return MODEL_MAPPER.map(find(id), UserDetailResponse.class);
  }

  @Override
  public UserDetailResponse getDetailByUsername(String username) {
    log.info("(getDetailByUsername)id: {}", username);

    User user = repository.findByUsername(username).orElseThrow(NotFoundException::new);
    return MODEL_MAPPER.map(user, UserDetailResponse.class);
  }

  @Override
  public UserResponse update(String id) {
    return null;
  }

  @Override
  public List<UserResponse> list(int page, int size, boolean isAll) {
    return null;
  }

  @Override
  public void remove(String id) {
    log.info("(remove)id: {}", id);
    repository.delete(find(id));
  }

  @Override
  public User find(String id) {
    log.info("(find)id: {}", id);
    return repository.findById(id).orElseThrow(NotFoundException::new);
  }


  private void validateExistByUserName(String username) {
    if (username.length() > 0 && repository.existsByUsername(username))
      throw new ConflictException();
  }

  private void validateExistByEmail(String email) {
    if (email.length() > 0 && repository.existsByEmail(email)) throw new ConflictException();
  }
}
