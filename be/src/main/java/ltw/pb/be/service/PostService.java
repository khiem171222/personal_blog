package ltw.pb.be.service;


import ltw.pb.be.dto.request.post.PostRequest;
import ltw.pb.be.dto.response.post.PostPageResponse;
import ltw.pb.be.dto.response.post.PostResponse;
import ltw.pb.be.entity.Post;
import ltw.pb.be.service.base.BaseService;


public interface PostService extends BaseService<Post> {
  PostResponse create(PostRequest request);

  PostPageResponse list(int pageNo, int pageSize, boolean isAll, String sortBy, String sortDir, String categoryId);

  PostResponse detail(String id);

  PostResponse update(String id, PostRequest request);

  void remove(String id);
}
