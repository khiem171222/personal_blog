package ltw.pb.be.utils;

public class StringUtils {
  public static boolean isNullOrEmpty(String words) {
    return words == null || words.isEmpty();
  }
}
