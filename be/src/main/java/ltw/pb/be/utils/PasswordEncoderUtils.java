package ltw.pb.be.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderUtils {
  public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
}
