package ltw.pb.be.constanst;

public class PersonalBlogConstants {

  public static class CommonConstants {
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String SUCCESS = "SUCCESS";

    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "10";
    public static final String DEFAULT_ISALL = "false";
    public static final String DEFAULT_SORT_BY = "createdAt";
    public static final String DEFAULT_SORT_DIRECTION = "DESC";
    public static final String BASE_NAME_MESSAGES = "classpath:messages";
    public static final String SYSTEM = "system";
    public static final String ANONYMOUS = "anonymous";
    public static final String LANGUAGE = "Accept-Language";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String EMPTY = "";
  }


  public static class StatusException {
    public static final Integer NOT_FOUND = 404;
    public static final Integer CONFLICT = 409;
    public static final Integer BAD_REQUEST = 400;
  }

  public static class MessageCode {
    public static final String CREATE_CATEGORY_SUCCESS = "Create category successfully";
    public static final String UPDATE_CATEGORY_SUCCESS = "Update category successfully";
    public static final String DELETE_CATEGORY_SUCCESS = "Delete category successfully";
  }

  public static class MessageException {

  }

  public static class AuthConstant {
    public static String TYPE_TOKEN = "Bear ";
    public static String AUTHORIZATION = "Authorization";
  }
}
