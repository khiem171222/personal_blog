package ltw.pb.be.security;


import lombok.RequiredArgsConstructor;
import ltw.pb.be.filter.JwtAuthenticationFilter;
import ltw.pb.be.security.error.UnAuthenticationCustomHandler;
import ltw.pb.be.security.error.UnAuthorizationCustomHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

  private final JwtAuthenticationFilter jwtAuthenticationFilter;
  private final UnAuthenticationCustomHandler unAuthenticationCustomHandler;
  private final UnAuthorizationCustomHandler unAuthorizationCustomHandler;


  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    return http
          .csrf().disable()
          .authorizeHttpRequests()
          .requestMatchers("api/v1/**")
          .permitAll()
          .anyRequest().permitAll()
          .and()
          .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
          .exceptionHandling()
          .accessDeniedHandler(unAuthorizationCustomHandler)
          .authenticationEntryPoint(unAuthenticationCustomHandler)
          .and().sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
          .and().build();
  }
}
