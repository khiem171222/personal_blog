package ltw.pb.be.repository;


import ltw.pb.be.entity.Post;
import ltw.pb.be.repository.base.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepository extends BaseRepository<Post> {
  boolean existsByTitle(String title);
  @Query("SELECT p FROM Post p " +
        "WHERE (:category_id IS NULL OR p.category.id = :category_id) ")
  List<Post> listPage(@Param("category_id") String categoryId, Pageable pageable);

  @Query("SELECT p FROM Post p " +
        "WHERE (:category_id IS NULL OR p.category.id = :category_id) ")
  List<Post> list(@Param("category_id") String categoryId);

  @Query("SELECT count(p) FROM Post p " +
        "WHERE (:category_id IS NULL OR p.category.id = :category_id) ")
  int countList(@Param("category_id") String categoryId);

}
