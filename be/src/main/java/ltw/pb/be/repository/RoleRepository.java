package ltw.pb.be.repository;


import ltw.pb.be.entity.Role;
import ltw.pb.be.repository.base.BaseRepository;

import java.util.Optional;

public interface RoleRepository extends BaseRepository<Role> {
  Optional<Role> findByName(String name);
}
