package ltw.pb.be.repository;


import ltw.pb.be.entity.Category;
import ltw.pb.be.repository.base.BaseRepository;

public interface CategoryRepository extends BaseRepository<Category> {
  boolean existsByName(String name);
}
