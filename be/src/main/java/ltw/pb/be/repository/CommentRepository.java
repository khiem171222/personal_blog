package ltw.pb.be.repository;

import ltw.pb.be.entity.Comment;
import ltw.pb.be.repository.base.BaseRepository;

import java.util.List;

public interface CommentRepository extends BaseRepository<Comment> {
  List<Comment> findByPostId(long postId);
}
