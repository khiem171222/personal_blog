package ltw.pb.be.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ltw.pb.be.service.JwtTokenService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import static ltw.pb.be.constanst.PersonalBlogConstants.AuthConstant.AUTHORIZATION;
import static ltw.pb.be.constanst.PersonalBlogConstants.AuthConstant.TYPE_TOKEN;


@Component
@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
  private final JwtTokenService jwtTokenService;

  @Override
  protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain filterChain
  ) throws ServletException, IOException {
    log.debug(
          "(doFilterInternal)request: {}, response: {}, filterChain: {}",
          request,
          response,
          filterChain
    );

    String accessToken = request.getHeader(AUTHORIZATION);
    if (Objects.isNull(accessToken)) {
      filterChain.doFilter(request, response);
      return;
    } else if (!accessToken.startsWith(TYPE_TOKEN)) {
      filterChain.doFilter(request, response);
      return;
    }

    var jwtToken = accessToken.substring(TYPE_TOKEN.length());
    var userid = jwtTokenService.getSubjectFromToken(jwtToken);
    var username = jwtTokenService.getUsernameFromToken(jwtToken);
    var grantedAuthority = getGrantedAuthority(jwtTokenService.getRoleFromToken(jwtToken));
    var usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
          username, userid, grantedAuthority
    );
    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    filterChain.doFilter(request, response);

  }

  private List<GrantedAuthority> getGrantedAuthority(Object roles) {
    log.info("(getGrantedAuthority) start");
    List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    return grantedAuthorities;
  }
}
